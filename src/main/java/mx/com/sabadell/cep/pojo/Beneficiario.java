package mx.com.sabadell.cep.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Beneficiario {

	@JsonProperty("Nombre")
	private String nombre;
	@JsonProperty("IVA")
	private String iVA;
	@JsonProperty("MontoPago")
	private String montoPago;
	@JsonProperty("TipoCuenta")
	private String tipoCuenta;
	@JsonProperty("Cuenta")
	private String cuenta;
	@JsonProperty("Concepto")
	private String concepto;
	@JsonProperty("RFC")
	private String rFC;
	@JsonProperty("BancoReceptor")
	private String bancoReceptor;

}
