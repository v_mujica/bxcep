package mx.com.sabadell.cep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BxcepApplication {

	public static void main(String[] args) {
		SpringApplication.run(BxcepApplication.class, args);
	}

}
