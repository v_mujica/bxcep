package mx.com.sabadell.cep.controller;

import lombok.extern.log4j.Log4j2;
import mx.com.sabadell.cep.dto.RequestValidation;
import mx.com.sabadell.cep.http.CepHttpClient;
import mx.com.sabadell.cep.pojo.SPEITercero;
import mx.com.sabadell.cep.tools.SimilarityTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.*;

@Log4j2
@RestController
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@RequestMapping("/consultas")
public class ConsultaCEPController {

    @Autowired
    private CepHttpClient cepHttpClient;

    @Autowired
    private SimilarityTool similarityTool;

    @CrossOrigin
    @RequestMapping(path = "/operacion", method = RequestMethod.POST)
    public SPEITercero getOperacion(@RequestBody RequestValidation requestValidation) {
        SPEITercero result = null;
        try {
            result = cepHttpClient.sendPOST(requestValidation);
            result.setCoincidencia(similarityTool.getCoincidencia(
                    result.getBeneficiario(),
                    requestValidation
            ));
        } catch (Exception ex) {
            log.error(ex);
            result = new SPEITercero();
        }
        return result;
    }

}
