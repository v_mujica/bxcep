package mx.com.sabadell.cep.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class BanxicoInstitution implements Serializable {
    private List<List<String>> instituciones;
    private List<List<String>> institucionesMISPEI;
    boolean overrideCaptcha;
}
