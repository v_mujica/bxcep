package mx.com.sabadell.cep.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;

@Data
public class SPEITercero {

    @JacksonXmlProperty(isAttribute = true)
    private String FechaOperacion;
    @JacksonXmlProperty(isAttribute = true)
    private String claveRastreo;
    @JacksonXmlProperty(isAttribute = true)
    private String cadenaCDA;
    @JacksonXmlProperty(isAttribute = true)
    private String Hora;
    @JacksonXmlProperty(isAttribute = true)
    private String numeroCertificado;
    @JacksonXmlProperty(isAttribute = true)
    private String sello;
    @JacksonXmlProperty(isAttribute = true)
    private String ClaveSPEI;
    @JsonProperty("Ordenante")
    private Ordenante ordenante;
    @JsonProperty("Beneficiario")
    private Beneficiario beneficiario;
    @JsonProperty("Coincidencia")
    private Coincidencia coincidencia;

}
