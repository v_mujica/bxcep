package mx.com.sabadell.cep.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestValidation {

    private String tipoCriterio;
    private String fecha;
    private String criterio;
    private String emisor;
    private String receptor;
    private String cuenta;
    private String receptorParticipante;
    private String monto;
    private String captcha;
    private String tipoConsulta;
    private String nombreBeneficiario;
    private String nombreReceptor;
}
