package mx.com.sabadell.cep.controller;

import lombok.extern.log4j.Log4j2;
import mx.com.sabadell.cep.dto.BanxicoInstitution;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@Log4j2
@RestController
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@RequestMapping("/banxico")
public class BanxicoInstitutionController {

    private static final String BANXICO_URL = "https://www.banxico.org.mx/cep/instituciones.do?fecha=%s";

    @CrossOrigin
    @GetMapping("/institution")
    public ResponseEntity<BanxicoInstitution> getInstitutions(@RequestParam("fecha") String fecha) {
        return new RestTemplate().getForEntity(String.format(BANXICO_URL, fecha), BanxicoInstitution.class);
    }
}
