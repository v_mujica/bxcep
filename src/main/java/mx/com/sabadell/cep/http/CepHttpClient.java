package mx.com.sabadell.cep.http;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.extern.log4j.Log4j2;
import mx.com.sabadell.cep.dto.RequestValidation;
import mx.com.sabadell.cep.pojo.SPEITercero;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@Component
public class CepHttpClient {
    public SPEITercero sendPOST(RequestValidation reqVal) throws IOException {

        log.info("#########################################################################################################################################################3##############");

        SPEITercero sPEITercero = null;
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CookieStore cookieStore = new BasicCookieStore();
        HttpContext httpContext = new BasicHttpContext();
        httpContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);

        HttpPost post1 = new HttpPost("https://www.banxico.org.mx/cep/valida.do");
        List<NameValuePair> urlParameters1 = new ArrayList<>();
        urlParameters1.add(new BasicNameValuePair("tipoCriterio", reqVal.getTipoCriterio()));
        urlParameters1.add(new BasicNameValuePair("fecha", reqVal.getFecha()));
        urlParameters1.add(new BasicNameValuePair("criterio", reqVal.getCriterio()));
        urlParameters1.add(new BasicNameValuePair("emisor", reqVal.getEmisor()));
        urlParameters1.add(new BasicNameValuePair("receptor", reqVal.getReceptor()));
        urlParameters1.add(new BasicNameValuePair("cuenta", reqVal.getCuenta()));
        urlParameters1.add(new BasicNameValuePair("receptorParticipante", reqVal.getReceptorParticipante()));
        urlParameters1.add(new BasicNameValuePair("monto", reqVal.getMonto()));
        urlParameters1.add(new BasicNameValuePair("captcha", reqVal.getCaptcha()));
        urlParameters1.add(new BasicNameValuePair("tipoConsulta", "0"));
        post1.setEntity(new UrlEncodedFormEntity(urlParameters1));

        HttpPost post2 = new HttpPost("https://www.banxico.org.mx/cep/valida.do");
        List<NameValuePair> urlParameters2 = new ArrayList<>();
        urlParameters2.add(new BasicNameValuePair("tipoCriterio", reqVal.getTipoCriterio()));
        urlParameters2.add(new BasicNameValuePair("fecha", reqVal.getFecha()));
        urlParameters2.add(new BasicNameValuePair("criterio", reqVal.getCriterio()));
        urlParameters2.add(new BasicNameValuePair("emisor", reqVal.getEmisor()));
        urlParameters2.add(new BasicNameValuePair("receptor", reqVal.getReceptor()));
        urlParameters2.add(new BasicNameValuePair("cuenta", reqVal.getCuenta()));
        urlParameters2.add(new BasicNameValuePair("receptorParticipante", reqVal.getReceptorParticipante()));
        urlParameters2.add(new BasicNameValuePair("monto", reqVal.getMonto()));
        urlParameters2.add(new BasicNameValuePair("captcha", reqVal.getCaptcha()));
        urlParameters2.add(new BasicNameValuePair("tipoConsulta", "1"));
        post2.setEntity(new UrlEncodedFormEntity(urlParameters2));

        CloseableHttpResponse response2 = httpClient.execute(post2);
        String resultPost2 = EntityUtils.toString(response2.getEntity());
        log.info("############## RESULT POST 2" + resultPost2 + "##############");

        HttpGet get = new HttpGet("https://www.banxico.org.mx/cep/descarga.do?formato=XML");
        get.setHeader("Content-Type", "text/xml; charset=UTF-8");

        CloseableHttpResponse response3 = httpClient.execute(get);
        XmlMapper xmlMapper = new XmlMapper();
        String resultGet = EntityUtils.toString(response3.getEntity());
        log.info("############## RESULT GET" + resultGet + "##############");

        log.info("############## parse XML ############## " + resultGet);
        sPEITercero = xmlMapper.readValue(resultGet, SPEITercero.class);
        response2.close();
        response3.close();
        httpClient.close();
        return sPEITercero;
    }


//	private Map<String, String> getFirstResponseHTML(String data) {
//		Map<String, String> response = new HashMap<>();
//		Document doc = Jsoup.parse(data);
//		Element resultTable = doc.select("#xxx").first();
//		if (resultTable != null) {
//			Elements rows = resultTable.select("tr");
//			for (int i = 0; i < rows.size(); i++) {
//			    Element row = rows.get(i);
//			    Elements cols = row.select("td");
//			    response.put(cols.get(0).text(), cols.get(1).text());
//			}
//		}
//		return response;
//	}


}