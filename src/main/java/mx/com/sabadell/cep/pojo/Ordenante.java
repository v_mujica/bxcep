package mx.com.sabadell.cep.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Ordenante {

	@JsonProperty("Nombre")
	private String nombre;
	@JsonProperty("BancoEmisor")
	private String bancoEmisor;
	@JsonProperty("TipoCuenta")
	private String tipoCuenta;
	@JsonProperty("Cuenta")
	private String cuenta;
	@JsonProperty("RFC")
	private String rFC;

}