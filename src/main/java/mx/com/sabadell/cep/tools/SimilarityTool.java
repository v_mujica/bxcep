package mx.com.sabadell.cep.tools;

import mx.com.sabadell.cep.dto.RequestValidation;
import mx.com.sabadell.cep.pojo.Beneficiario;
import mx.com.sabadell.cep.pojo.Coincidencia;
import org.apache.commons.text.similarity.JaroWinklerSimilarity;
import org.springframework.stereotype.Component;

@Component
public class SimilarityTool {

    public Coincidencia getCoincidencia(Beneficiario beneficiario, RequestValidation requestValidation) {
        return Coincidencia.builder()
                .nombreCoincidencia(getCoincidencia(beneficiario.getNombre(), requestValidation.getNombreBeneficiario()))
                .bancoCoincidencia(getCoincidencia(beneficiario.getBancoReceptor(), requestValidation.getNombreReceptor()))
                .cuentaCoincidencia(getCoincidencia(beneficiario.getCuenta(), requestValidation.getCuenta()))
                .build();
    }

    private String getCoincidencia(String string1, String string2) {
        string1 = string1.replaceAll("[^a-zA-Z\\s+]", "");
        string2 = string2.replaceAll("[^a-zA-Z\\s+]", "");
        JaroWinklerSimilarity winklerSimilarity = new JaroWinklerSimilarity();
        Double apply = winklerSimilarity.apply(string1, string2);
        return String.format("%.2f%%", apply * 100);
    }
}
